
import java.util.Scanner;

/**
 * Classe que implementa operacions matemàtiques
 *
 * @author Miriam Flores Diéguez
 */
public class Calculadora {

    private static double num1;
    private static double num2;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix el primer número: ");
        num1 = scanner.nextDouble();

        System.out.println("Introdueix el segon número: ");
        num2 = scanner.nextDouble();

        System.out.println("Suma: " + sumar(num1, num2));

    }

    public static double sumar(double a, double b) {
        return a + b;
    }

    public static double restar(double a, double b) {
        return a - b;
    }

    public static double multiplicar(double a, double b) {
        return a * b;
    }

    public static double dividir(double a, double b) throws ArithmeticException {
         if (b == 0) {
                  throw new ArithmeticException("El divisor no pot ser zero");
         }
         return a / b;
    }
}
